<?php

// primery settings
	add_action( 'after_setup_theme', function() {
		add_theme_support( 'title-tag' );
		add_theme_support('post-thumbnails');

		register_nav_menus( array(
			'aside-nav' => 'menu',
		) );
		add_image_size ('small', 78, 64, true);
		add_image_size ('full-size', 1900, 900, true);
	} );

// add svg files

	add_filter( 'upload_mimes', 'upload_allow_types' );
	function upload_allow_types( $mimes ) {
		// разрешаем новые типы
		$mimes['svg'] = 'image/svg+xml';
		return $mimes;
	};

// add style and script
	function pfeiler_scripts() {
		
		wp_enqueue_style( 'style-css', get_stylesheet_uri() );


		wp_enqueue_script('jquery');
		wp_enqueue_script('custom', get_template_directory_uri() . '/js/main.js', array('jquery'), '', true);
	}
	add_action( 'wp_enqueue_scripts', 'pfeiler_scripts' );

		// add options page
	if( function_exists('acf_add_options_page') ) {
	
		acf_add_options_page(array(
			'page_title' 	=> 'options site',
			'menu_title'	=> 'options themes',
			'menu_slug' 	=> 'theme-general-settings',
			'capability'	=> 'edit_posts',
			'redirect'		=> false
		));
		
		acf_add_options_sub_page(array(
			'page_title' 	=> 'settings gallery',
			'menu_title'	=> 'gallery',
			'parent_slug'	=> 'theme-general-settings',
		));
		
		acf_add_options_sub_page(array(
			'page_title' 	=> 'settings sidebar',
			'menu_title'	=> 'sidebar',
			'parent_slug'	=> 'theme-general-settings',
		));
	}

	//add new post type
	add_action( 'init', 'create_post_types' );
	function create_post_types(){
		register_post_type('gallery', array(
			'label'  => null,
				'labels' => array(
					'name'               => 'gallery', // основное название для типа записи
					'singular_name'      => 'gallery', // название для одной записи этого типа
					'add_new'            => 'add new', // для добавления новой записи
					'add_new_item'       => 'add new item', // заголовка у вновь создаваемой записи в админ-панели.
					'edit_item'          => 'edit item', // для редактирования типа записи
					'new_item'           => 'new item', // текст новой записи
					'view_item'          => 'view item', // для просмотра записи этого типа.
				),
				'description'         => '',
				'public'              => true,
				'publicly_queryable'  => null, // зависит от public
				'exclude_from_search' => null, // зависит от public
				'show_ui'             => null, // зависит от public
				'show_in_menu'        => null, // показывать ли в меню адмнки
				'show_in_admin_bar'   => null, // по умолчанию значение show_in_menu
				'show_in_nav_menus'   => null, // зависит от public
				'show_in_rest'        => null, // добавить в REST API. C WP 4.7
				'rest_base'           => null, // $post_type. C WP 4.7
				'menu_position'       => null,
				'menu_icon'           => 'dashicons-format-gallery',
				//'capability_type'   => 'post',
				//'capabilities'      => 'post', // массив дополнительных прав для этого типа записи
				//'map_meta_cap'      => null, // Ставим true чтобы включить дефолтный обработчик специальных прав
				'hierarchical'        => false,
				'supports'            => array('title','editor', 'thumbnail'), // 'title','editor','author','thumbnail','excerpt','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats'
				'taxonomies'          => array('gallery-category'),
				'has_archive'         => false,
				'rewrite'             => true,
				'query_var'           => true,
		) );
	}

	//register taxonomy

	add_action('init', 'create_taxonomy');
	function create_taxonomy(){
		// список параметров: http://wp-kama.ru/function/get_taxonomy_labels
		register_taxonomy('gallery-category', array('gallery'), array(
			'label'                 => '', // определяется параметром $labels->name
			'labels'                => array(
				'name'              => 'gallery-category',
				'singular_name'     => 'category',
				'search_items'      => 'Search category',
				'all_items'         => 'All category',
				'view_item '        => 'View category',
				'parent_item'       => 'Parent category',
				'parent_item_colon' => 'Parent category:',
				'edit_item'         => 'Edit category',
				'update_item'       => 'Update category',
				'add_new_item'      => 'Add New category',
				'new_item_name'     => 'New category Name',
				'menu_name'         => 'category',
			),
				'description'           => '', // описание таксономии
				'public'                => true,
				'publicly_queryable'    => null, // равен аргументу public
				'hierarchical'          => false,
				'rewrite'               => true,
			) );

	}
?>