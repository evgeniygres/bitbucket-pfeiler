'use strict';

var gulp 		 = require('gulp'),
	sass 		 = require('gulp-sass'),
	plumber      = require('gulp-plumber'),
	sourcemaps   = require('gulp-sourcemaps'),
	browserSync  = require('browser-sync'),
	autoprefixer = require('gulp-autoprefixer'),
	cssmin       = require('gulp-cssmin'),
	del          = require('del'),
	rename       = require('gulp-rename'),
	imagemin     = require('gulp-imagemin'),
	pngquant     = require('imagemin-pngquant-gfw'),
	cache        = require('gulp-cache'),
	uglify       = require('gulp-uglify'),
	svgSprite    = require('gulp-svg-sprite'),
	concat       = require('gulp-concat');




gulp.task('sass', function () {
	return gulp.src('app/scss/**/*.scss')
	.pipe(sass())
	.pipe(sourcemaps.init())
	.pipe(plumber())
	.pipe(autoprefixer(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], {cascade: true }))
	.pipe(sourcemaps.write())
	.pipe(gulp.dest('app/'))
	.pipe(browserSync.reload({stream: true}));
});

gulp.task('browser-sync', function() {
	browserSync({
		server: {
			baseDir: 'app'
		},
		browser: "firefox",
		notify: false 
	});
});


gulp.task('svgSprite', function () {
    return gulp.src('app/svg/icons/*.svg') // svg files for sprite
        .pipe(svgSprite({
                mode: {
                    stack: {
                        sprite: "../sprite.svg"  //sprite file name
                    }
                },
            }
        ))
        .pipe(gulp.dest('app/svg/'));
});



gulp.task('img', function() {					
	return gulp.src('app/img/**/*')
	.pipe(cache(imagemin({
		interlaced: true,
		progressive: true,
		svgoPlugins: [{removeViewBox: false}],
		une:[pngquant({ quality: '65-80', speed: 4 })]
	})))
	.pipe(gulp.dest('app/img'));
});



gulp.task('watch',['browser-sync', 'img'], function() {
	gulp.watch('app/scss/**/*.scss', ['sass']);
	gulp.watch('app/*.php', browserSync.reload);
	gulp.watch('app/*.html', browserSync.reload);
	gulp.watch('app/js/**/*.js', browserSync.reload);
});







