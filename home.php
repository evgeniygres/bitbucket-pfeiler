<?php
 /* 
 	Template name: Home Page  
 	Template Post Type: post, gallery
 */
 ?>
<?php get_header(); ?>
	<div class="wrapper">
		<div class="mobile-nav">
			<span></span>
		</div>
	    <a class="logo" href="/">
	        <svg class="icon icon-logo">
	            <use xlink:href="svg/sprite.svg#logo"></use>
	        </svg>
	    </a>
		<?php get_sidebar(); ?>
		<?php get_template_part( 'parts/gallery' ); ?>
	</div>	
<?php get_footer(); ?>