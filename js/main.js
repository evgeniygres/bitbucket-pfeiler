jQuery(document).ready(function() {
	//Mobile navigation
    var aside = jQuery('.aside');
    var touchLink = jQuery('.mobile-nav');

	jQuery(touchLink).on('click', function() {
    	jQuery(aside).toggleClass('show-menu');
    	jQuery(this).toggleClass('close-button');
    });
});
