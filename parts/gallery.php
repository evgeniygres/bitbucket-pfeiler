<div class="gallery">

    <?php
    $posts = get_posts( array(
        'numberposts' => 0,
        'post_type'   => 'gallery',
        'suppress_filters' => true,
    ) );

    foreach( $posts as $post ){
        setup_postdata($post);
        ?>
        <div class="gallery-item">
		<?php if( get_field('gallery-img') ): ?>
			<img class="gallery__img" alt="galery" src="<?php the_field('gallery-img'); ?>">
			<div class="gallery__description">
				<h2 class="gallery__description-title"><?php the_field('gallery-description-title'); ?></h2><span class="gallery__description-autor"><?php the_field('gallery-description-autor'); ?></span>
			</div>
		</div>
	<?php endif; ?>
    <?php
    }

    wp_reset_postdata();
    ?>
</div>