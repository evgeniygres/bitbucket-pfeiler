<aside class="aside">
    <?php
    
    $logo = get_field('aside-logo', 'option');
    if($logo) : ?>
        <a class="aside-logo" href="/">
            <svg class="icon icon-logo">
                <use xlink:href="<?php the_field('aside-logo', 'option'); ?>"></use>
            </svg>
        </a>
    <?php endif;

    ?>
    <nav>
        <ul>
            <li><a class="aside-nav active" href="/">Gallery</a>
                <ul class="aside-subnav">
                    <li><a class="aside-nav-item" href="/">Oil</a></li>
                    <li><a class="aside-nav-item" href="/">Aquarel</a></li>
                    <li><a class="aside-nav-item" href="/">Portrait</a></li>
                    <li><a class="aside-nav-item" href="/">Autumn</a></li>
                    <li><a class="aside-nav-item" href="/">Sale</a></li>
                    <li><a class="aside-nav-item" href="/">Custom</a></li>
                </ul>
            </li>
            <li><a class="aside-nav" href="/">EXHIBITIONS</a></li>
            <li><a class="aside-nav" href="/">Portrait</a></li>
            <li><a class="aside-nav" href="/">About Us</a></li>
        </ul>
    </nav>
    <ul class="aside-social-links">
        <li><a href="#">
                <svg class="icon icon-facebook">
                    <use xlink:href="svg/sprite.svg#facebook"></use>
                </svg>
            </a>
        </li>
        <li><a href="#">
                <svg class="icon icon-instagram">
                    <use xlink:href="svg/sprite.svg#instagram"></use>
                </svg>
            </a>
        </li>
        <li><a href="#">
                <svg class="icon icon-pinterest">
                    <use xlink:href="svg/sprite.svg#pinterest"></use>
                </svg>
            </a>
        </li>
    </ul>
</aside>